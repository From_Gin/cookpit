
const isProduct = process.env.NODE_ENV !== 'development'

module.exports = {
  publicPath: './',
  outputDir: 'dist',
  //productionSourceMap: !isProduct,
  runtimeCompiler: true,
  pluginOptions: {
    //externals: isProduct ? externals : {}
  }
}