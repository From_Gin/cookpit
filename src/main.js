import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import iView from 'iview';
import 'iview/dist/styles/iview.css';
Vue.use(iView);
import animated from 'animate.css';
Vue.use(animated);
import './assets/css/font.css'
import './assets/icon/iconfont.css'
let echarts = require('echarts');
Vue.prototype.$echarts = echarts;
let echartsGL = require('echarts-gl');
Vue.prototype.$echartsGL = echartsGL;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
