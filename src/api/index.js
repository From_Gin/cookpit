
import axios from '@/utils/request';

export const getAllYear = (data, options) => {
	return axios.get('datacenter/project/GetYear', data, options);
}

export const getPlanAreaYear = (data, options) => {
	return axios.get('datacenter/project/GetPlannedRequisitionedAreaOverYears', data, options);
}

export const getPlanProportionHouse= (data, options) => {
	return axios.get('datacenter/project/GethouseholdsPercentage', data, options);
}

export const getPlanProportionArea= (data, options) => {
	return axios.get('datacenter/project/GetAcreagePercentage', data, options);
}

export const getPlanProportionProject= (data, options) => {
	return axios.get('datacenter/project/GetProjectPercentage', data, options);
}

export const getProjectPassedArea= (data, options) => {
	return axios.get('datacenter/project/GetProjectCompletionRateArea', data, options);
}

export const getProjectPassedHouse= (data, options) => {
	return axios.get('datacenter/project/GetProjectCompletionRateHouseholds', data, options);
}

export const getProjectStart= (data, options) => {
	return axios.get('datacenter/project/GetProjectStartRateInEachDistrict', data, options);
}

export const getMapData= (data, options) => {
	return axios.get('datacenter/project/GetAreaData', data, options);
}


export const getMapPlanArea= (data, options) => {
	return axios.get('datacenter/project/GetDataTotal', data, options);
}

export const getSearchArea= (data, options) => {
	return axios.get('datacenter/project/GetSearchArea', data, options);
}
//各区项目完成率
export const getAreaProjectCompletion= (data, options) => {
	return axios.get('datacenter/project/GetAreaSigningRate', data, options);
}
//区域年计划征收面积
export const getExpropriatedArea= (data, options) => {
	return axios.get('datacenter/project/GetExpropriatedArea', data, options);
}
//区域计划启动比
export const getAreaPlanStart= (data, options) => {
	return axios.get('datacenter/project/GetAreaPlanStart', data, options);
}

export const getAreaDataTotal= (data, options) => {
	return axios.get('datacenter/project/GetAreaDataTotal', data, options);
}