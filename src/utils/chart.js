let echarts = require('echarts');
export const linechart = (data) => {
    var seriesArr=data.data.map((item,index)=>{
       var seriesItem={
            name: '',
            type: 'line',
            smooth: 0.6,
            itemStyle: {
                normal: {
                    color: "#fff",
                    lineStyle: {
                        color: "#05A697"
                    }
                }
            },
            symbol: 'circle',
            symbolSize: 6,
            areaStyle: {
                normal: {
                    color:new echarts.graphic.LinearGradient(0,0,0,0.8,[
                        {
                            offset:0,
                            color:'#05A697'
                        },
                        {
                            offset:1,
                            color:'#162444'
                        },
                    ])
                }
            },
            data: item.data
        }
        return seriesItem;
    });
    var myChart=echarts.init(document.getElementById(data.id));
    myChart.setOption({
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'line',
                label: {
                    backgroundColor: '#10C2F6'
                }
            },
            formatter: '{a} {b}年：{c}'
        },
        color:['rgb(19,173,223)','rgb(19,223,96)'],
        grid: {
            left: '20px',
            right: '15px',
            bottom: '20px',
            top: '50px',
            containLabel: true
        },
        xAxis: [
            {
                type: 'category',
                name:'',
                boundaryGap: false,
                data: data.label,
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: '#fff'
                    }
                },
                axisLine:{
                    show: true,
                    lineStyle:{
                        color:'#224A80'
                    }
                },
                splitLine:{show: false},
                axisTick: {show: false}
            }
        ],
        yAxis: [
            {
                type: 'value',
                //min: 100000,
                //max: 500000,
                //interval:100000,
                name: "单位/万㎡",
                nameTextStyle: {
                    color: "#fff",
                    padding: [0, 40, 15, 0],
                    nameLocation: "start",
                },
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: '#fff',
                    }
                },
                axisLine:{
                    show: true,
                    lineStyle:{
                        color:'#224A80',
                        shadowOffsetY:-40,
                        shadowColor:'#224A80'
                    }
                },
                splitLine:{
                    show: true,
                    lineStyle:{
                        color:'#224A80'
                    }
                },
                axisTick: {show: false}
            }
        ],
        series:seriesArr
    },true);
    myChart.resize();
}

export const piechart = (data) => {
    var dataArr=data.data;
    var myChart=echarts.init(document.getElementById(data.id));
    myChart.setOption({
        tooltip: {
            trigger: 'item',
            formatter: '{a} {b}: {c}%'
        },
        color:["#2FC4E8","#37A1D9","#97BFFD","#837AEB","#9B95F3","#E8C0F0","#E891CF","#FD7193","#FF9F6D","#FDDB65"],
        legend:[
            {
                orient: 'vertical',
                itemHeight: 16,
                itemWidth: 16,
                data: data.label1,
                textStyle: {
                    color: '#fff'
                },
                left:"right",
                padding:[50,143,0,0],
                itemGap:26
            },
            {
                orient: 'vertical',
                itemHeight: 16,
                itemWidth: 16,
                data: data.label2,
                textStyle: {
                    color: '#fff'
                },
                left:"right",
                padding:[50,43,0,0],
                itemGap:26
            },
        ],
        grid: {
            left: '0px',
            right: '0',
            bottom: '0',
            top: '10px',
            containLabel: true
        },
        series: [
            {
                name: '',
                type: 'pie',
                radius: ['0%', '70%'],
                center: ["28%", "45%"], 
                avoidLabelOverlap: false,
                label: {
                    show: false,
                    position: 'inner'
                },
                emphasis: {
                    label: {
                        show: false,
                        fontSize: '12',
                        fontWeight: 'bold'
                    }
                },
                labelLine: {
                    show: false
                },
                data: dataArr
            }
        ]
    },true);
}

export const circlechart = (data) => {
    var dataArr=data.data;
    var myChart=echarts.init(document.getElementById(data.id));
    myChart.setOption({
        tooltip: {
            show:false,
            trigger: 'item',
            formatter: '{a} {b}: {c}%'
        },
        color:["rgb(248,226,105)","rgb(6,149,243)"],
        legend:[
            {
                orient: 'horizontal',
                itemHeight: 16,
                itemWidth: 16,
                data: data.label,
                textStyle: {
                    color: '#fff'
                },
                x: 'center', // 'center' | 'left' | {number},
                y: 'bottom', // 'center' | 'bottom' | {number}
                padding:[0,0,50,0],
            }
        ],
        series: [
            {
                name: '',
                type: 'pie',
                radius: ['45%', '70%'],
                center: ["50%", "40%"], 
                avoidLabelOverlap: false,
                label: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: '24',
                        //fontWeight: 'bold',
                        formatter: "{c}%",
                        textStyle: {
                            color: '#fff',
                        }
                    }
                },
                labelLine: {
                    show: false
                },
                data: dataArr
            }
        ]
    },true);
}

export const rectchart = (data) => {
    var seriesArr=data.data.map((item)=>{
       var seriesItem={
            name:item.label,
            type:'bar',
            data:item.data,
            itemStyle:{
                normal: {
                    label: {
                        show: true,		//开启显示
                        position: 'top',	//在上方显示
                        textStyle: {	    //数值样式
                            color: '#fff',
                            fontSize: 12
                        },
                        formatter: function(params) {
                            var value=params.value;
                            return value+'%';
                        }
                    },
                    color:new echarts.graphic.LinearGradient(0,0,0,1,[
                        {
                            offset:0,
                            color:'rgb(89,200,241)'
                        },
                        {
                            offset:1,
                            color:'rgb(21,104,250)'
                        },
                    ]),
                    barBorderRadius: [18, 18, 0 ,0]
                }

            },
            barWidth:12
        }
        return seriesItem;
    });
    var myChart=echarts.init(document.getElementById(data.id));
    myChart.setOption({
        legend: {
            show:false,
            itemHeight: 14,
            itemWidth: 14,
            data:[],
            textStyle: {
                color: '#13ADDF'
            },
            left:"left",
            padding:[20,0,0,0]
        },
        grid: {
            left: '0',
            right: '0',
            bottom: '15px',
            top: '30px',
            containLabel: true
        },
        tooltip: {
            trigger: 'axis',
            formatter: '{b}: {c}%'
        },
        xAxis: {
            type: 'category',
            data: data.label,
            axisLabel: {
                show: true,
                textStyle: {
                    color: '#fff',
                },
                interval:0,
                formatter:function(value){  
                    var ret = ""; 
                    var maxLength = 3;
                    value=value.trim();
                    var valLength = value.length; 
                    var rowN = Math.ceil(valLength / maxLength);
                    if (rowN > 1){  
                        for (var i = 0; i < 2; i++) {  
                            var temp = "";  
                            var start = i * maxLength;  
                            var end = start + maxLength;  
                            temp = value.substring(start, end) + "\n";  
                            ret += temp;
                        }  
                        return ret;  
                    }else {  
                        return value;  
                    }  
                }
            },
            axisLine:{
                show: true,
                lineStyle:{
                    color:'#224A80'
                }
            },
            splitLine:{show: false},
            axisTick: {show: false}
        },
        yAxis: {
            type:'value',
            axisLabel: {
                show: true,
                textStyle: {
                    color: '#fff',
                }
            },
            axisLine:{
                show:false,
                lineStyle:{
                    color:'#224A80'
                }
            },
            splitLine:{
                show: true,
                lineStyle:{
                    color:'#224A80'
                }
            },
            axisTick: {show: false},
        },
        series: seriesArr
    },true);
}

export const rect3dchart = (data) => {
    var clientWidth=document.documentElement.clientWidth || document.body.clientWidth;
    var clientHeight=document.documentElement.clientHeight || document.body.clientHeight;
    var width=(clientWidth-60)*0.3;
    var height=(clientHeight-101-40-6)*0.5-80;
    var devNum=width/clientWidth;
    //console.log(clientWidth);
    if(clientWidth>=2000){
        width=(clientWidth-60)*0.3*devNum;
        height=height*0.25;
    }else{
        width=(clientWidth-60)*0.3*devNum*1.5;
        height=height*0.35;
    }
    var myChart=echarts.init(document.getElementById(data.id));
    myChart.setOption({
            tooltip: {
                type: 'axis',
                formatter: function(params) {
                    var value=params.value;
                    var name=value[0];
                    var num=value[2]+'%';
                    var msg=name+'：'+num;
                    return msg;
                }
            },
            xAxis3D: {
                type: 'category',
                name: '',
                data: data.xdata,
                axisTick: {
                    show: true
                },
                nameTextStyle: {
                    //color: that.echartsColor.textColor,
                },
                lineStyle: {
                    color: "#224A80",
                    width: 1,
                    type: "solid",
                },
                axisLine: {
                    lineStyle: {
                        color: "#224A80",
                    },
                },
                axisTick: {
                    // 坐标轴 刻度
                    show: false, // 是否显示
                    inside: true, // 是否朝内
                    length: 3, // 长度
                    lineStyle: {
                    // 默认取轴线的样式
                        color: "#224A80",
                        width: 1,
                        type: "solid",
                    },
                },
                axisLabel: {
                    // 坐标轴标签
                    show: true, // 是否显示
                    inside: false, // 是否朝内
                    rotate: 50, // 旋转角度
                    margin: 15, // 刻度标签与轴线之间的距离
                    color: "#fff", // 默认取轴线的颜色,
                    interval: 0
                },
                splitLine: {
                    // gird区域中的分割线
                    show: false, // 是否显示
                    lineStyle: {
                        color: "#224A80",
                    },
                },
            },
            yAxis3D: {
                type: 'category',
                name: '',
                data: data.ydata,
                // axisTick: {
                //     show: false
                // },
                axisLine: {
                    lineStyle: {
                        color: '#224A80'
                    }
                },
                nameTextStyle: {
                    color: "",
                },
                lineStyle: {
                    color: '#224A80',
                    width: 1,
                    type: "solid",
                },
                axisLine: {
                    lineStyle: {
                        color: '#224A80',
                    },
                },
                axisTick: {
                    // 坐标轴 刻度
                    show: false, // 是否显示
                    inside: true, // 是否朝内
                    length: 3, // 长度
                    lineStyle: {
                        // 默认取轴线的样式
                        color: '#224A80',
                        width: 1,
                        type: "solid",
                    },
                },
                axisLabel: {
                    // 坐标轴的标签
                    show: false, // 是否显示
                    inside: false, // 是否朝内
                    rotate: 0, // 旋转角度
                    margin: 0, // 刻度标签与轴线之间的距离
                    color: "#fff", // 默认轴线的颜色
                    formatter: "{value}",
                },
                splitLine: {
                    // gird区域中的分割线
                    show: false, // 是否显示
                    lineStyle: {
                        color: "#224A80",
                    },
                },
            },
            zAxis3D: {
                type: 'value',
                name: '',
                // axisTick: {
                //     show: false
                // },
                nameTextStyle: {
                //color: that.echartsColor.textColor,
                },
                lineStyle: {
                    color: "#224A80",
                    width: 1,
                    type: "solid",
                },
                axisLine: {
                    lineStyle: {
                        color: "rgb(17,147,255)",
                    },
                },
                axisTick: {
                    // 坐标轴 刻度
                    show: false, // 是否显示
                    inside: true, // 是否朝内
                    length: 3, // 长度
                    lineStyle: {
                        // 默认取轴线的样式
                        color: "",
                        width: 1,
                        type: "solid",
                    },
                },
                axisLabel: {
                    // 坐标轴标签
                    show: true, // 是否显示
                    inside: false, // 是否朝内
                    rotate: 20, // 旋转角度
                    margin: 18, // 刻度标签与轴线之间的距离
                    color: "#fff", // 默认取轴线的颜色
                },
                splitLine: {
                    // gird区域中的分割线
                    show: true, // 是否显示
                    lineStyle: {
                        color: "#224A80",
                    },
                },
            },
            grid3D: {
                boxWidth: width,
                boxHeight: height,
                boxDepth: 50,
                zlevel: -50,
                axisPointer: {
                    show: false
                },
                viewControl:{
                    beta:0,
                    //distance:200,
                    alpha:0
                },
                light: {
                    main: {
                        intensity: 1.2
                    },
                    ambient: {
                        intensity: 0.3
                    }
                }
            },
            title: {
                text: ''
            },
            series: [{
                type: 'bar3D',
                name: '',
                barSize: 8,
                data:  data.zData,
                label: {
                    show: true,
                    textStyle: {
                        fontSize: 10,
                        color:'#fff'
                    },
                    formatter: function (params){
                        var value=params.value;
                        var val=value[2]+'%';
                        return val;
                    }
                },
                itemStyle: {
                    color:'rgb(17,147,255)'
                },
                emphasis: {
                    label: {
                        textStyle: {
                            show:true,
                            fontSize: 12,
                            color: '#fff'
                        }
                    }
                }
            }]
    });

}


export const cycledrow = (target,callback) => {
    var targetDom=document.getElementById(target);
    setInterval(function(){
        targetDom.removeAttribute("_echarts_instance_");
        callback();
    },300000);
   
}

