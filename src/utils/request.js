import HttpRequest from './axios';
import config from '../config';
const baseUrl = config.baseUrl;

const axios = new HttpRequest(baseUrl, 5000);
export default axios;
