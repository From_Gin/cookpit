import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../views/Index.vue'
import Area from '../views/Area.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },
  {
    path: '/area/:name/:id',
    name: 'Area',
    component: Area
  },
]

const router = new VueRouter({
  //mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
